#NECESITAS JAVA 8
#BORRAR TABLAS DE LA BD
#MODIFICAR /src/main/resources/application.properties
brew install maven
mvn clean install
docker build -t pets-container .
docker run -d -p 8080:8080 --network=host pets-container

curl localhost:8080/api/add -d name=croqueta -d owner=pedro -d species=perro -d sex=m
curl localhost:8080/api/add -d name=quesadilla -d owner=pedro -d species=gato -d sex=f

#EL API TIENE 3 ENDPOINTS

#/api/add	POST

/api/{name}	GET

/api/all	GET

#localhost:8080/api/pet/croqueta
{
    "id": 1,
    "name": "croqueta",
    "owner": "daryl",
    "species": "perro",
    "sex": "m"
}
